/**
 * REFER THE LINKS GIVEN BELOW FOR FURTHER HELP
 * https://bluejeans.github.io/api-rest-meetings/site/index.html
 * https://support.bluejeans.com/s/article/How-to-Create-Users-Using-the-BlueJeans-Meetings-API-Endpoints
 */

var request = require("request");
var express = require("express");
var bodyParser = require("body-parser");
require("dotenv").config();
var async = require("async");

var app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

/*
***API for Creating session with Bluejeans***
req = {
  "username"  : "email address",
   "password"  : "password"
}
*/
app.post("/createSession", (req, res) => {
  var options = {
    grant_type: "password",
    username: req.body.username,
    password: req.body.password,
  };

  request.post(
    { uri: "https://api.bluejeans.com/oauth2/token?Password", json: options },
    (err, response, body) => {
      if (response.statusCode !== 200) {
        // console.log(err.message);
        res.status(response.statusCode).send("error creating session");
      } else {
        let createSessionBody = body;
        request.get(
          {
            uri: `https://api.bluejeans.com/v1/user/${body.scope.user}/enterprise_profile?access_token=${createSessionBody.access_token}`,
          },
          (err, response, body) => {
            if (response.statusCode !== 200) {
              res
                .status(response.statusCode)
                .send("error getting enterprise number");
            } else {
              body = JSON.parse(body);
              let responseData = {
                createSessionBody,
                enterprise: body,
              };
              res.send(responseData);
            }
          }
        );
      }
    }
  );
});

/*
*** API for creating meeting ***
req = {
  "title": "YOUR MEETING TITLE",
  "description": "YOUR MEETING DESCRIPTION",
  "access_token":"ACCESS TOKEN FROM /createSession API createSessionBody object",
  "user":"USER FROM /createSession API"
  "startdate":"START DATE CONNVERTED TO INTEGER" // use Date().getTime 
  "enddate":"END DATE CONVERTED TO INTEGER" // use Date().getTime 
}
*/
app.post("/setMeeting", (req, res) => {
  var options = {
    title: req.body.title,
    description: req.body.description,
    start: req.body.startdate ? req.body.startdate : 1588185000000,
    end: req.body.enddate ? req.body.enddate : 1590777000000,
    timezone: "Asia/Kolkata",
    addAttendeePasscode: true,
    endPointVersion: "2.10",
    endPointType: "WEB_APP",
    sendEmail:true,
    isPersonalMeeting: true,
    attendees: [
      {
        email: "pranjalmalkhede@gmail.com",
      },
      {
        email:"pranjal8793@outlook.com"
      }
    ],
    advancedMeetingOptions: {
      autoRecord: false,
      muteParticipantsOnEntry: false,
      encryptionType: "NO_ENCRYPTION",
      moderatorLess: true,
      videoBestFit: true,
      disallowChat: false,
      publishMeeting: true,
      showAllAttendeesInMeetingInvite: true,
    },
  };
  //   console.log(options);

  request.post(
    {
      uri: `https://api.bluejeans.com/v1/user/${req.body.user}/scheduled_meeting/?access_token=${req.body.access_token}&email=true&isPersonalMeeting=true`,
      json: options,
    },
    (err, response, body) => {
      if (response.statusCode !== 201) {
        // console.log(err.message);
        res.status(response.statusCode).send("error setting meeting");
      } else {
        //  console.log(res.body);
        var responseData = {
          URL: `http://bluejeans.com/${body.numericMeetingId}/${body.attendeePasscode}`,
          ...body,
        };
        res.send(responseData);
      }
    }
  );
});

/*
***API for adding new user***
req = {
    "emailId": "UNIQUE EMAIL ID",
    "username": "USERNAME",
    "password": "PASSWORD",
    "firstName": "FIRSTNAME",
    "lastName": "LASTNAME",
    "company": "COMPANY NAME",
    "title": "JOB POSITION",
    "enterprise":"enterprise number FROM /createSession API enterprise object",
    "access_token":"ACCESS TOKEN FROM /createSession API createSessionBody object",
    "isAdmin":"true" //default true
}
*/
app.post("/addUser", (req, res) => {
  var options = {
    emailId: req.body.emailId,
    username: req.body.username,
    password: req.body.password,
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    company: req.body.company,
    title: req.body.title,
  };

  request.post(
    {
      uri: `https://api.bluejeans.com/v1/enterprise/${req.body.enterprise}/users?access_token=${req.body.access_token}&forcePasswordChange=false&isAdmin=${req.body.isAdmin}`,
      json: options,
    },
    (err, response, body) => {
      if (!body.id) {
        //  console.log(err.message)
        res.status(404).send("error in registering user");
        // throw new Error(err)
      } else {
        //  console.log(body)
        request.post(
          {
            url: `https://api.bluejeans.com/v1/user/${body.id}/room?access_token=${req.body.access_token}`,
            json: {},
          },
          (err, response, body) => {
            if (response.statusCode !== 201) {
              // console.log(err.message)
              res
                .status(response.statusCode)
                .send("error in setting default setting for user");
              // throw new Error(err)
            } else {
              // console.log(body)
              res.send(body);
            }
          }
        );
      }
    }
  );
});

/*
***API for LIVE session with Bluejeans***
req = {
  "enterprise"  : "enterprise number from /createSession API enterprise object",
   "access_token"  : "access token from /createSession API createSessionBody object"
}
*/
app.post("/getLiveMeetInfo", (req, res) => {
  request.get(
    {
      uri: `https://api.bluejeans.com/v1/enterprise/${req.body.enterprise}/indigo/meetings/live?access_token=${req.body.access_token}`,
    },
    (err, response, body) => {
      if (response.statusCode !== 200) {
        res.status(response.statusCode).send(response);
      } else {
        res.send(body);
      }
    }
  );
});

/**
 * API to get user details
 * req = {
 *  "enterprise":"enterprise number from /createSesssion API enterprise object",
 *  "access_token":"access_token  from /createSesssion API object"
 * }
 */
app.post("/getUsers", (req, res) => {
  request.get(
    {
      uri: `https://api.bluejeans.com/v1/enterprise/${req.body.enterprise}/users?access_token=${req.body.access_token}`,
    },
    (err, response, body) => {
      if (response.statusCode !== 200) {
        res.status(response.statusCode).send("error getting users");
      } else {
        let users = JSON.parse(body).users;
        const promises = users.map((user) =>
          getUsersDetails(user, req.body.access_token)
        );
        Promise.all(promises).then((data) => {
          res.json(data);
        });
      }
    }
  );
});

getUsersDetails = (user, access_token) => {
  return new Promise((resolve, reject) => {
    request.get(
      {
        uri: `https://api.bluejeans.com/v1/user/${user.id}?access_token=${access_token}`,
      },
      (err, response, body) => {
        if (response.statusCode !== 200) {
          reject(body);
        } else {
          resolve(JSON.parse(body));
        }
      }
    );
  });
};

/**
 * API fot getting scheduled meetings for any user
 * req = {
 * "user":"user number"
 * "access_token":"access_token"
 * 
 * }
 */
app.post('/listScheduledMeet',(req,res)=>{
  request.get({uri:`https://api.bluejeans.com/v1/user/${req.body.user}/scheduled_meeting?access_token=${req.body.access_token}`},(err,response,body)=>{
    if(response.statusCode!==200){
      res.status(response.statusCode).json(response)
    }
    else{
      res.json(body)
    }
})
})

/**
 * API fot getting history of meetings for any user
 * req = {
 * "user":"user number"
 * "access_token":"access_token"
 * 
 * }
 */
app.post('/listHistoryMeet',(req,res)=>{
  request.get({uri:`https://api.bluejeans.com/v1/user/${req.body.user}/meeting_history?access_token=${req.body.access_token}`},(err,response,body)=>{
    if(response.statusCode!=200){
      res.status(response.statusCode).json("error getting history")
    }
    else {
      res.send(body)
    }
  })
})



// getEnterpriseNumber = (url) => {
//     return new Promise((resolve, reject)=> {
//         request.get(url, (error, response, body) => {
//             if (!error) {
//                 // console.log('getEnterpriseNumber',body)
//                 resolve(body);
//             } else {
//                 reject(error);
//             }
//         });
//     });
// }

var server = app.listen(process.env.PORT || 3000, () => {
  console.log("server running on " + server.address().port);
});
